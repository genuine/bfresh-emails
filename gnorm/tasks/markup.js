var gulp = require('gulp'),
    include = require('gulp-include'),
    inlineCss = require('gulp-inline-css'),
    config = require('../config').markup;

gulp.task('markup', function() {
  return gulp.src(config.src)
    .pipe(inlineCss({
      applyStyleTags: true,
      applyLinkTags: true,
      removeStyleTags: true,
      removeLinkTags: true
    }))
    .pipe(gulp.dest(config.dest));
});
