#INSTRUCTIONS ON USING THE TEMPLATE

#CSS

    Create all of your CSS in the styles.css file.
    
    SASS is not allowed.
    
    Once you have completed your styles do the following:
    
    1. Remove the link to the stylesheet in the head.
    2. Create a <style type="text/css"></style> block.
    3. Copy and paste your CSS into the style block.
    4. Go to https://inliner.cm/
    5. Copy the entire contents of index.html into the field provided on the site. 
       Please note, we are not using the site to do the email campaign, only for it's inlining function
       So don't feel like you have to make an account. The inlining function is free to use.
    6. Click the "Make Inline" button.
    7. Copy the returned content.
    8. Paste into your index.html file.
    9. Remove the <style></style> block where you copied your CSS into the index.html file.
    10. Preview in a web browser to ensure your layout styles were inlined properly.
    11. Profit!
    
#Gotchas
    
    You will notice a <center> tag. This is needed due to the sheer volume of email clients and how they parse email.
    Yes this has been deprecated... for web browsers. Email clients tend to be both ahead and behind the curve. 
    
    This is a responsive email template.
    
    Mind your closing tags. HTML Email is insane to get the layout just right and Outlook complicates matters. 
    
    If you wish to see how to use the one and two column layouts, there is a folder with a sample email built. Look at
    the code to view how I used the tables.
    
    Divs within table cells are allowed.
    
    Background Images are not allowed
    
    This template can be adapted to most layouts by following the same principles.
    
    Obviously there will be different max-widths to use. Changing the webkit, outer and two-column column max-width is 
    what will be needed to sort that out 
    
#TODO
    Add three-column layout and four-column layout
    